let font
let cursor
let points = []
let segments = []
let shapes = []
let editorMode = true
let all_selected = false
let colorPicker

let gradient
let pattern
let img
let buffer
let gradient2
let bg = true

let Engine = Matter.Engine,
  World = Matter.World,
  Runner = Matter.Runner,
  Bodies = Matter.Bodies,
  Composite = Matter.Composite,
  MouseConstraint = Matter.MouseConstraint,
  Mouse = Matter.Mouse,
  Body = Matter.Body,
  Events = Matter.Events,
  Composites = Matter.Composites,
  Common = Matter.Common

let engine
let world

let exploded = false

let boundaries = {
  ground: undefined,
  ceiling: undefined,
  left_wall: undefined,
  right_wall: undefined
}
let editorInfoBox = {
  body: undefined,
  pos: {
    x: 20,
    y: 20
  },
  width: 310,
  height: 290,
  is_collided: false
}
let canvas
let data
let level_data = {
  level_1: undefined,
  level_2: undefined,
  level_3: undefined,
  level_4: undefined,
  level_5: undefined,
  current_level: 1
}

let array
let N = 25
let SP = -50
let DT = 0.1
let EPS = 0.00000001
let DAMPING = 0.95
let DELTA = 100
let KGrid = 10.0
let KClick = 4.0

let inc = 0.1
let scl = 32
let start = 0
let n_scale = 0.1
let cols = NaN
let rows = NaN
let palettes = [
  [
    [7, 59, 76],
    [255, 209, 102],
    [236, 136, 5],
    [6, 214, 160],
    [17, 229, 236]
  ],
  // [
  //   [205, 214, 208],
  //   [214, 203, 193],
  //   [214, 169, 154],
  //   [225, 96, 54],
  //   [227, 23, 10]
  // ],
  // [
  //   [135, 130, 130],
  //   [132, 220, 207],
  //   [166, 217, 247],
  //   [188, 204, 224],
  //   [191, 152, 160]
  // ]
]
let palette = []

function spring_force(ax, ay, bx, by, k) {
  var xx = ax - bx
  var yy = ay - by

  var d = dist(xx, yy, 0, 0)

  var nx = xx / (d + EPS)
  var ny = yy / (d + EPS)

  var f = k * d

  var fx = f * nx
  var fy = f * ny

  return createVector(fx, fy)
}

class Dot {
  constructor(i, j) {
    this.vx = 0
    this.vy = 0

    this.x = map(i, 0, N - 1, SP, width - SP)
    this.y = map(j, 0, N - 1, SP, height - SP)

    this.x0 = this.x
    this.y0 = this.y
  }

  update() {
    let d = dist(mouseX, mouseY, this.x, this.y)
    let delta = DELTA

    let intensity = KClick * exp(-d * d / (delta * delta))

    let res = createVector(0, 0)
    res.add(spring_force(mouseX, mouseY, this.x, this.y, intensity))
    if (mouseIsPressed) {}
    res.add(spring_force(this.x0, this.y0, this.x, this.y, KGrid))

    this.vx += DT * res.x
    this.vy += DT * res.y

    this.vx *= DAMPING
    this.vy *= DAMPING

    this.x += DT * this.vx
    this.y += DT * this.vy
  }
}

function randint(min, max) {
  // min and max should are inclusive
  return Math.floor(Math.random() * Math.floor(1 + max - min)) + min
}

function preload() {
  font = loadFont('fonts/Foulmouth.otf')
  // font = loadFont('fonts/Foulmouth Italic.otf')
  img = loadImage('pattern/pattern6.jpg')
  level_data.level_1 = loadJSON('data/level_1.json')
  level_data.level_2 = loadJSON('data/level_2.json')
  level_data.level_3 = loadJSON('data/level_3.json')
  level_data.level_4 = loadJSON('data/level_4.json')
  level_data.level_5 = loadJSON('data/level_5.json')
  data = level_data.level_1
}

function setup() {
  canvas = createCanvas(windowWidth, windowHeight)
  colorMode(RGB, 255, 255, 255, 1)
  textAlign(LEFT, CENTER)
  rectMode(CORNER)
  ellipseMode(CENTER)

  cursor = new Cursor()

  colorPicker = createColorPicker('#ffffff')
  colorPicker.position(170, 210)

  // gradient = createRadialGradient(0, dist(0, 0, windowWidth, windowHeight) * 0.5, windowWidth * 0.5, windowHeight * 0.5)
  // gradient2 = createRadialGradient(0, dist(0, 0, windowWidth, windowHeight) * 0.5, windowWidth * 0.5, windowHeight * 0.5)
  // // gradient.colors(0, 'yellow', 0.25, 'orange', 0.5, 'red', 1, 'magenta')
  // gradient2.colors(0, color(255), 1, color(0))
  // gradient.colors(0, color(0), 1, color(255))
  pattern = createPattern(img)

  engine = Engine.create()
  world = engine.world
  const runner = Runner.create()
  Runner.run(runner, engine)

  // ADD EDITOR INFO BOX
  editorInfoBox.body = Bodies.rectangle(editorInfoBox.pos.x + editorInfoBox.width * 0.5, editorInfoBox.pos.y + editorInfoBox.height * 0.5, editorInfoBox.width, editorInfoBox.height, {
    isStatic: true
  })
  // ADD GROUND
  boundaries.ground = Bodies.rectangle(width / 2, height * 1.5, width * 2, height, {
    isStatic: true
  })
  World.add(world, boundaries.ground)
  // ADD CEILING
  boundaries.ceiling = Bodies.rectangle(width / 2, -0.5 * height, width * 2, height, {
    isStatic: true
  })
  World.add(world, boundaries.ceiling)
  // ADD LEFT WALL
  boundaries.left_wall = Bodies.rectangle(-0.5 * width, height / 2, width, height * 2, {
    isStatic: true
  })
  World.add(world, boundaries.left_wall)
  // ADD RIGHT WALL
  boundaries.right_wall = Bodies.rectangle(1.5 * width, height / 2, width, height * 2, {
    isStatic: true
  })
  World.add(world, boundaries.right_wall)

  for (let i = 0; i < Object.keys(data).length; i++) {
    shapes.push(new Polygon(data[i].points))
  }

  engine.gravity.y = 0

  let mouse = Mouse.create(canvas.elt)
  let mouseConstraint = MouseConstraint.create(engine, {
    mouse: mouse,
    constraint: {
      stiffness: 0.2,
      render: {
        visible: false
      }
    }
  })

  Composite.add(world, mouseConstraint)

  // world.bodies[4].frictionAir = 0.1
  // world.bodies[4].friction = -0.4

  // buffer = createGraphics(32, 32)
  // buffer.pixelDensity(1)
  // buffer.background(153)
  // buffer.fill(230)
  // buffer.noStroke()
  // buffer.rect(0, 0, 16, 16)
  // buffer.rect(16, 16, 16, 16)
  // pattern = createPattern(buffer)

  array = new Array(N)
  for (let i = 0; i < N; i++) {
    array[i] = new Array(N)
  }

  for (let i = 0; i < N; i++) {
    for (let j = 0; j < N; j++) {
      array[i][j] = new Dot(i, j)
    }
  }
  buffer = createGraphics(windowWidth, windowHeight)

  // pick one of 3 colors
  palette_choice = randint(0, palettes.length - 1)
  palette = palettes[palette_choice]

  cols = windowWidth / scl
  rows = windowHeight / scl
  noiseDetail(2)
}

function draw() {

  // gradient2.colors(0, color(255 * abs(sin(frameCount * 0.01))), 1, color(255 * abs(cos(frameCount * 0.01))))

  // gradient.colors(0, color(255 * abs(cos(frameCount * 0.01))), 1, color(255 * abs(sin(frameCount * 0.01))))

  // fillGradient(gradient2)
  // noStroke()
  // rect(0, 0, windowWidth, windowHeight)


  // if (bg) {
  //   background(13, 45, 254, 0.1)
  // } else {
  //   background(22, 0)
  //   document.body.style.background = '#0d2dfe'
  // }

  // image(img, 0, 0)

  // fill(0, 0.85)
  // noStroke()
  // rect(0, 0, windowWidth, windowHeight)


  // GRID DISTORTION
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // fillPattern(pattern)
  // noStroke()
  // rect(0, 0, windowWidth, windowHeight)

  // if (mouseIsPressed) {
  //   KClick = 12.0
  // } else {
  //   KClick = 4.0
  // }

  // buffer.background(0)
  // buffer.push()
  // for (let i = 0; i < N; i++) {
  //   for (let j = 0; j < N; j++) {
  //     array[i][j].update()
  //   }
  // }

  // for (let i = 0; i < N - 1; i++) {
  //   for (let j = 0; j < N - 1; j++) {
  //     let d1 = array[i][j]
  //     let d2 = array[i + 1][j]
  //     let d3 = array[i][j + 1]

  //     buffer.stroke(255)
  //     buffer.strokeWeight(4)
  //     buffer.line(d1.x, d1.y, d2.x, d2.y)
  //     buffer.line(d1.x, d1.y, d3.x, d3.y)
  //   }
  // }

  // // for (let i = 0; i < N - 1; i++) {
  // //   let d1 = array[N - 1][i]
  // //   let d2 = array[N - 1][i + 1]
  // //   let d3 = array[i][N - 1]
  // //   let d4 = array[i + 1][N - 1]

  // //   buffer.stroke(255)
  // //   buffer.strokeWeight(4)
  // //   buffer.line(d1.x, d1.y, d2.x, d2.y)
  // //   buffer.line(d3.x, d3.y, d4.x, d4.y)
  // // }
  // buffer.pop()
  // pattern = createPattern(buffer)
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // NOISE CARPET
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // buffer.push()
  // buffer.blendMode(DIFFERENCE)
  // buffer.clear()
  // buffer.background(palette[0])
  // let yoff_0 = cos(start)
  // let xoff_0 = sin(start)

  // let yoff_1 = cos(start)
  // let xoff_1 = sin(start)

  // for (let x = 0; x < cols; x++) {
  //   for (let y = 0; y < rows; y++) {
  //     let n1 = noise(xoff_0 / 5, yoff_0) * 255
  //     let n2 = noise(xoff_1 / 10, yoff_1) * 255

  //     // // 'stuttered' blocks
  //     // if (n1 <= 96) {
  //     //   c1 = palette[1]
  //     //   c2 = palette[2]
  //     //   r = map(x, 0, rows, c1[0], c2[0])
  //     //   g = map(x, 0, rows, c1[1], c2[1])
  //     //   b = map(x, 0, rows, c1[2], c2[2])
  //     //   buffer.fill(r, g, b)
  //     //   buffer.rect(x * scl, y * scl, scl, scl / 2)
  //     // }

  //     // full blocks
  //     if (n2 <= 96) {
  //       c1 = palette[3]
  //       c2 = palette[4]
  //       r = map(y, 0, cols, c1[0], c2[0])
  //       g = map(y, 0, cols, c1[1], c2[1])
  //       b = map(y, 0, cols, c1[2], c2[2])
  //       buffer.fill(r, g, b)
  //       buffer.rect(x * scl, y * scl, scl, scl)
  //     }

  //     xoff_0 += inc
  //     xoff_1 -= inc
  //   }
  //   yoff_0 += inc
  //   yoff_1 -= inc
  // }
  // start += radians(inc)
  // buffer.pop()
  // fillPattern(pattern)
  // rect(0, 0, windowWidth, windowHeight)
  // pattern = createPattern(buffer)
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  background(0)

  // push()
  // cursor.render_radar()
  // pop()

  for (p of points) {
    push()
    translate(p.x, p.y)
    noFill()
    stroke(255, 0, 0)
    strokeWeight(6)
    point(0, 0)
    pop()
  }

  for (let i = 0; i < points.length - 1; i++) {
    stroke(128)
    strokeWeight(2)
    noFill()
    line(points[i].x, points[i].y, points[i + 1].x, points[i + 1].y)
  }

  for (p of shapes) {
    p.render_origin()
  }

  for (p of shapes) {
    p.update()
    p.render()
  }

  cursor.is_collided = false
  for (p of shapes) {
    if (p.is_collided) {
      cursor.is_collided = true
      break
    }
  }
  if (collidePointRect(mouseX, mouseY, editorInfoBox.pos.x, editorInfoBox.pos.y, editorInfoBox.width, editorInfoBox.height)) {
    cursor.is_collided = true
    editorInfoBox.is_collided = true
  } else {
    editorInfoBox.is_collided = false
  }

  push()
  blendMode(DIFFERENCE)
  cursor.render()
  cursor.update()

  // stroke(255)
  // strokeWeight(4)
  // noFill()
  // line(mouseX, mouseY, pmouseX, pmouseY)
  pop()

  fill(64)
  noStroke()
  rect(editorInfoBox.pos.x, editorInfoBox.pos.y, editorInfoBox.width, editorInfoBox.height)

  fill(255)
  textFont(font)
  textSize(20)
  text('E - toggle editor mode (' + editorMode + ')', 34, 32 + 10)
  text('ESC - exit current draw process', 34, 62 + 10)
  text('BACKSPACE - undo last point', 34, 92 + 10)
  text('DEL - delete selected shape', 34, 122 + 10)
  text('ENTER - finish shape', 34, 152 + 10)
  let selection_state
  if (all_selected) {
    selection_state = '(all selected)'
  } else {
    selection_state = ''
  }
  text('A - select all / none ' + selection_state, 34, 182 + 10)
  text('current color: ', 34, 212 + 10)
  text('press 0-5 for selecting a level', 34, 242 + 10)
  text('current level: ' + level_data.current_level, 34, 272 + 10)
}

// function mouseClicked() {
//   noiseSeed(frameCount)
//   var temp_choice = palette_choice
//   palette_choice = Math.floor(Math.random() * palettes.length)
//   while (palette_choice == temp_choice) {
//     palette_choice = Math.floor(Math.random() * palettes.length)
//   }
//   palette = palettes[palette_choice]
// }

function mousePressed() {}

function mouseWheel(event) {
  // p.tempPlane = p.plane + event.delta * player.scrollSpeed
  for (p of shapes) {
    if (p.is_collided) {
      p.body.angle += event.delta * 0.00001
      // p.body.angularVelocity = 0
      // p.body.angularSpeed = 0
    }
  }
}

addEventListener('mousedown', (event) => {
  // LEFT mouse button
  if (event.button === 0) {
    if (editorMode && !cursor.is_collided) {
      // PLACE NEW POINT ON CANVAS
      points.push(new Point(cursor.pos.x, cursor.pos.y))
    }
  }
  // RIGHT mouse button
  if (event.button === 2) {}
})

addEventListener('mouseup', (event) => {
  // LEFT mouse button
  if (event.button === 0) {}
  // RIGHT mouse button
  if (event.button === 2) {}
})

function keyPressed() {
  if (editorMode) {

    // CREATE NEW SHAPE
    if (keyCode === 13) {
      if (points.length >= 3) {
        shapes.push(new Polygon(points))
      }
      points = []
    }
    // ESCAPE CURRENTLY EDITED PROCESS
    if (keyCode === 27) {
      points = []
    }
    // REMOVE LAST PLACED POINT
    if (keyCode === 8) {
      if (points.length > 1) {
        points.pop()
      } else {
        points = []
      }
    }
    // DELETE HOVERED SHAPE
    if (keyCode === 46) {
      if (all_selected) {
        for (let i = 0; i < shapes.length; i++) {
          shapes[i].remove_from_world()
        }
        shapes = []
      } else {
        for (let i = 0; i < shapes.length; i++) {
          if (shapes[i].is_collided) {
            shapes[i].remove_from_world()
            shapes.splice(i, 1)
          }
        }
      }
    }
    // CONSOLE.LOG SHAPES TO JSON
    if (keyCode === 83) {
      let shape_data = []
      for (p of shapes) {
        shape_data.push(p.points)
      }
      data = JSON.stringify(shapes, getCircularReplacer())
      console.log(data)
      // console.log(JSON.stringify(shape_data))
    }
    // SELECT ALL / NONE
    if (keyCode === 65) {
      all_selected = !all_selected
    }
  } else {
    // SELECT IN-BETWEEN LEVEL
    if (keyCode === 48) {
      for (let i = 0; i < shapes.length; i++) {
        shapes[i].remove_from_world()
      }
      shapes = []
      level_data.current_level = 0
      exploded = false
    }
    // SELECT LEVEL 1
    if (keyCode === 49) {
      for (let i = 0; i < shapes.length; i++) {
        shapes[i].remove_from_world()
      }
      shapes = []
      data = undefined
      data = level_data.level_1
      level_data.current_level = 1
      exploded = false
      for (let i = 0; i < Object.keys(data).length; i++) {
        shapes.push(new Polygon(data[i].points))
      }
    }
    // SELECT LEVEL 2
    if (keyCode === 50) {
      for (let i = 0; i < shapes.length; i++) {
        shapes[i].remove_from_world()
      }
      shapes = []
      data = undefined
      data = level_data.level_2
      level_data.current_level = 2
      exploded = false
      for (let i = 0; i < Object.keys(data).length; i++) {
        shapes.push(new Polygon(data[i].points))
      }
    }
    // SELECT LEVEL 3
    if (keyCode === 51) {
      for (let i = 0; i < shapes.length; i++) {
        shapes[i].remove_from_world()
      }
      shapes = []
      data = undefined
      data = level_data.level_3
      level_data.current_level = 3
      exploded = false
      for (let i = 0; i < Object.keys(data).length; i++) {
        shapes.push(new Polygon(data[i].points))
      }
    }
    // SELECT LEVEL 4
    if (keyCode === 52) {
      for (let i = 0; i < shapes.length; i++) {
        shapes[i].remove_from_world()
      }
      shapes = []
      data = undefined
      data = level_data.level_4
      level_data.current_level = 4
      exploded = false
      for (let i = 0; i < Object.keys(data).length; i++) {
        shapes.push(new Polygon(data[i].points))
      }
    }
    // SELECT LEVEL 5
    if (keyCode === 53) {
      for (let i = 0; i < shapes.length; i++) {
        shapes[i].remove_from_world()
      }
      shapes = []
      data = undefined
      data = level_data.level_5
      level_data.current_level = 5
      exploded = false
      for (let i = 0; i < Object.keys(data).length; i++) {
        shapes.push(new Polygon(data[i].points))
      }
    }
    // SHAKE SCENE
    if (keyCode === 13) {
      shake_scene(engine)
      if (!exploded) {
        setTimeout(function(){
          exploded = true
        }, 1000)
      }
    }
  }
  // TOGGLE EDITOR MODE
  if (keyCode === 69) {
    editorMode = !editorMode
    points = []
  }
  if (keyCode === 66) {
    bg = !bg
  }
}

function keyReleased() {}

function shake_scene(engine) {
  var bodies = Composite.allBodies(engine.world)
  for (var i = 0; i < bodies.length; i++) {
    var body = bodies[i]
    if (!body.isStatic) {
      var forceMagnitude = 0.05 * body.mass
      Body.applyForce(body, body.position, {
        x: (forceMagnitude + Common.random() * forceMagnitude) * Common.choose([1, -1]),
        y: (forceMagnitude + Common.random() * forceMagnitude) * Common.choose([1, -1])
      })
    }
  }
}

class Cursor {
  constructor() {
    this.pos = createVector(windowWidth * 0.5, windowHeight * 0.5, 0)
    this.size = {
      width: 10,
      height: 16,
      gap: 8
    }
    this.color = color(0, 196, 128, 1)
    this.precision = false
    this.movement = createVector(0, 0, 0)
    this.is_collided = false
    this.radar = 0
  }

  render() {
    push()
    rectMode(CENTER)
    translate(this.pos.x, this.pos.y)
    noStroke()
    fill(this.color)
    // UPPER CURSOR PART
    beginShape()
    vertex(0, -this.size.gap)
    vertex(-0.5 * this.size.width, -this.size.gap - 0.666 * this.size.width)
    vertex(-0.5 * this.size.width, -this.size.height - this.size.gap)
    vertex(0.5 * this.size.width, -this.size.height - this.size.gap)
    vertex(0.5 * this.size.width, -this.size.gap - 0.666 * this.size.width)
    endShape(CLOSE)
    // BOTTOM CURSOR PART
    beginShape()
    vertex(0, this.size.gap)
    vertex(-0.5 * this.size.width, this.size.gap + 0.666 * this.size.width)
    vertex(-0.5 * this.size.width, this.size.height + this.size.gap)
    vertex(0.5 * this.size.width, this.size.height + this.size.gap)
    vertex(0.5 * this.size.width, this.size.gap + 0.666 * this.size.width)
    endShape(CLOSE)
    // LEFT CURSOR PART
    beginShape()
    vertex(-this.size.gap, 0)
    vertex(-this.size.gap - 0.666 * this.size.width, -0.5 * this.size.width)
    vertex(-this.size.gap - this.size.height, -0.5 * this.size.width)
    vertex(-this.size.gap - this.size.height, 0.5 * this.size.width)
    vertex(-this.size.gap - 0.666 * this.size.width, 0.5 * this.size.width)
    endShape(CLOSE)
    // RIGHT CURSOR PART
    beginShape()
    vertex(this.size.gap, 0)
    vertex(this.size.gap + 0.666 * this.size.width, -0.5 * this.size.width)
    vertex(this.size.gap + this.size.height, -0.5 * this.size.width)
    vertex(this.size.gap + this.size.height, 0.5 * this.size.width)
    vertex(this.size.gap + 0.666 * this.size.width, 0.5 * this.size.width)
    endShape(CLOSE)
    pop()
  }

  render_radar() {
    let radius = dist(windowWidth, windowHeight, 0, 0) * 0.5
    let opacity = map(radius * this.radar, 0, radius, 1, 0)
    fill(64, 128, 64, opacity)
    // strokeWeight(9)
    // stroke(64, 128, 64)
    noStroke()
    circle(this.pos.x, this.pos.y, this.radar * radius)
  }

  update() {
    this.pos.x = mouseX
    this.pos.y = mouseY
    this.size.gap = abs(sin(frameCount * 0.05)) * 8

    this.radar += deltaTime * 0.0005
    if (this.radar >= 1) {
      this.radar = 0
    }
  }
}

class Point {
  constructor(x, y) {
    this.x = x
    this.y = y
  }
}

class Segment {
  constructor(p1, p2) {
    this.p1 = p1
    this.p2 = p2
  }
}

class Polygon {
  constructor(points, color) {
    this.points = points
    this.is_collided = false
    this.a = this.region(this.points)
    this.center = this.centroid()
    this.body = Bodies.fromVertices(this.center.x, this.center.y, this.points)
    World.add(world, this.body)
    this.trans_points
    this.color = color
    this.solved = false
    this.threshold = {
      position: 6,
      angle: 0.05,
      angularVelocity: 0.001,
      angularSpeed: 0.001
    }
  }

  render_origin() {
    if (!this.solved) {
      stroke(64)
      setLineDash([8, 8])
      strokeWeight(4)
      noFill()
      // stroke(255, 0.25)
      // fill(0)
      beginShape()
      for (p of this.points) {
        vertex(p.x, p.y)
      }
      endShape(CLOSE)
    }
  }

  render() {
    var pos = this.body.position
    var angle = this.body.angle

    push()
    translate(pos.x, pos.y)
    rotate(angle)
    translate(-this.center.x, -this.center.y)

    if (this.solved) {
      fill(32, 128, 0)
    } else {
      if (this.is_collided) {
        fill(196)
      } else {
        fill(128)
      }
    }
    // fillPattern(pattern)
    // fillGradient(gradient)
    // fill(128)
    noStroke()
    beginShape()
    for (p of this.points) {
      vertex(p.x, p.y)
    }
    endShape(CLOSE)

    // noFill()
    // stroke(0)
    // strokeWeight(8)
    // point(this.center.x, this.center.y)

    pop()

    // stroke(255)
    // strokeWeight(2)
    // noFill()    
    // beginShape()
    // for (p of this.trans_points) {
    //   vertex(p.x, p.y)
    // }
    // endShape(CLOSE)
  }

  update() {
    this.trans_points = this.recompute_points()
    if (all_selected) {
      this.is_collided = true
    } else {
      this.is_collided = collidePointPoly(cursor.pos.x, cursor.pos.y, this.trans_points)
    }
    if (editorMode) {
      this.body.isStatic = true
    } else {
      this.body.isStatic = false
    }
    if (this.is_solved()) {
      this.solved = true
      this.body.angle = 0
      this.body.position.x = this.center.x
      this.body.position.y = this.center.y
      // this.body.inverseInertia = 0
      // this.body.inverseMass = 0
      Matter.Body.setStatic(this.body, true)
      Matter.Sleeping.set(this.body, true)
      this.body.mass = Infinity
      this.body.inertia = Infinity
      this.body.density = Infinity
    }
  }

  is_solved() {
    let v1 = createVector(0, 1, 0)
    let v2 = createVector(sin(this.body.angle), cos(this.body.angle), 0)
    let angle = v1.angleBetween(v2)
    angle = abs(angle)
    if (dist(this.body.position.x, this.body.position.y, this.center.x, this.center.y) <= this.threshold.position && angle < this.threshold.angle && exploded && this.body.angularSpeed < this.threshold.angularSpeed && this.body.angularVelocity < this.threshold.angularVelocity && !this.body.isStatic) {
      return true
    }
  }

  recompute_points() {
    let pos = this.body.position
    let angle = this.body.angle
    let trans_points = []
    for (let i = 0; i < this.points.length; i++) {
      let vector = createVector(this.points[i].x - this.center.x, this.points[i].y - this.center.y)
      let rot = vector.rotate(angle)
      let translate_vector = createVector(rot.x + pos.x, rot.y + pos.y)
      trans_points.push(new Point(translate_vector.x, translate_vector.y))
    }
    return trans_points
  }

  remove_from_world() {
    World.remove(world, this.body)
  }

  compute_area() {
    this.area = 0
    for (let i = 0; i < this.points.length - 1; i++) {
      let v = this.points[i]
      let vn = this.points[i + 1]
      this.area += (v.x * vn.y - vn.x * v.y) / 2
    }
    return this.area
  }

  compute_center() {
    let cx = 0,
      cy = 0
    for (let i = 0; i < this.points.length - 1; i++) {
      let v = this.points[i]
      let vn = this.points[i + 1]
      cx += (v.x + vn.x) * (v.x * vn.y - vn.x * v.y) / (6 * this.area)
      cy += (v.y + vn.y) * (v.x * vn.y - vn.x * v.y) / (6 * this.area)
    }
    this.center = new Point(cx, cy)
    return this.center
  }

  region(points) {
    this.points = points || []
    this.length = points.length
  }

  area() {
    var area = 0,
      i,
      j,
      point1,
      point2
    for (i = 0, j = this.length - 1; i < this.length; j = i, i += 1) {
      point1 = this.points[i]
      point2 = this.points[j]
      area += point1.x * point2.y
      area -= point1.y * point2.x
    }
    area /= 2
    return area
  }

  centroid() {
    var x = 0,
      y = 0,
      i,
      j,
      f,
      point1,
      point2
    for (i = 0, j = this.length - 1; i < this.length; j = i, i += 1) {
      point1 = this.points[i]
      point2 = this.points[j]
      f = point1.x * point2.y - point2.x * point1.y
      x += (point1.x + point2.x) * f
      y += (point1.y + point2.y) * f
    }
    f = this.area() * 6
    return new Point(x / f, y / f)
  }
}

function setLineDash(list) {
  drawingContext.setLineDash(list)
  drawingContext.lineDashOffset = frameCount / 4
}

function windowResized() {
  // let 
  resizeCanvas(windowWidth, windowHeight)

  Body.scale(boundaries.ground, 1, 1)
  Body.scale(boundaries.ceiling, 1, 1)
  Body.scale(boundaries.left_wall, 1, 1)
  Body.scale(boundaries.right_wall, 1, 1)
  Body.translate(boundaries.ground, {x: 0, y: 0})
  Body.translate(boundaries.ceiling, {x: 0, y: 0})
  Body.translate(boundaries.left_wall, {x: 0, y: 0})
  Body.translate(boundaries.right_wall, {x: 0, y: 0})
}

const getCircularReplacer = () => {
  const seen = new WeakSet()
  return (key, value) => {
    if (typeof value === "object" && value !== null) {
      if (seen.has(value)) {
        return
      }
      seen.add(value)
    }
    return value
  }
}