/**************************************************
 * https://bleuje.com/p5js-myprojects/grid-distortion/index.html
 * 
 * Spring grid distortion by Etienne Jacob, 2018
 * ************************************************/

var N = 50
var SP = 50
var DT = 0.1
var EPS = 0.00000001
var DAMPING = 0.95
var DELTA = 80
var KGrid = 10.0
var KClick = 15.0

function spring_force(ax, ay, bx, by, k) {
  var xx = ax - bx
  var yy = ay - by

  var d = dist(xx, yy, 0, 0)

  var nx = xx / (d + EPS)
  var ny = yy / (d + EPS)

  var f = k * d

  var fx = f * nx
  var fy = f * ny

  return createVector(fx, fy)
}

class Dot {
  constructor(i, j) {
    this.vx = 0
    this.vy = 0

    this.x = map(i, 0, N - 1, SP, width - SP)
    this.y = map(j, 0, N - 1, SP, height - SP)

    this.x0 = this.x
    this.y0 = this.y
  }

  update() {
    let d = dist(mouseX, mouseY, this.x, this.y)
    let delta = DELTA

    let intensity = KClick * exp(-d * d / (delta * delta))

    let res = createVector(0, 0)
    if (mouseIsPressed) {
      res.add(spring_force(mouseX, mouseY, this.x, this.y, intensity))
    }
    res.add(spring_force(this.x0, this.y0, this.x, this.y, KGrid))

    this.vx += DT * res.x
    this.vy += DT * res.y

    this.vx *= DAMPING
    this.vy *= DAMPING

    this.x += DT * this.vx
    this.y += DT * this.vy
  }
}

function draw_connection(d1, d2) {
  stroke(255)
  strokeWeight(4)
  line(d1.x, d1.y, d2.x, d2.y)
}

// var array

function setup() {
  createCanvas(windowWidth, windowHeight)
  background(0)

  // array = new Array(N)
  // for (let i = 0; i < N; i++) {
  //   array[i] = new Array(N)
  // }

  // for (let i = 0; i < N; i++) {
  //   for (let j = 0; j < N; j++) {
  //     array[i][j] = new Dot(i, j)
  //   }
  // }
}

function draw() {
  background(0)

  push()
  for (let i = 0; i < N; i++) {
    for (let j = 0; j < N; j++) {
      array[i][j].update()
    }
  }

  for (let i = 0; i < N - 1; i++) {
    for (let j = 0; j < N - 1; j++) {
      let d1 = array[i][j]
      let d2 = array[i + 1][j]
      let d3 = array[i][j + 1]
      draw_connection(d1, d2)
      draw_connection(d1, d3)
    }
  }

  for (let i = 0; i < N - 1; i++) {
    let d1 = array[N - 1][i]
    let d2 = array[N - 1][i + 1]
    let d3 = array[i][N - 1]
    let d4 = array[i + 1][N - 1]
    draw_connection(d1, d2)
    draw_connection(d3, d4)
  }
  pop()
}